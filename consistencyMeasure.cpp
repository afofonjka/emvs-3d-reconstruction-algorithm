#include "firstCloud.cpp"
#include "Eigen/SVD"
#include "octree.h"



using namespace std;

/*
global variables used for octree creation and normals computation
*/

map<int, vector<emvs::Point3D> > octreePoints;
map<int, int> cameraIdx;
mutex octreeMutex;
float** maximums;
float** minimums;
pthread_mutex_t *mutexArray;
int totalSize;
ANNpointArray dataPts;
int *octreeColors;
int *octreeImages;
int octreeCounter;
ANNkd_tree *kdTree;



float* pointIn3DSpace(int myCamera, int x, int y, float depth)
{
	float p1[3];
	float* t = (float*)malloc(3 * sizeof(float));
	p1[0] = -1 * (x - cameras[myCamera]->w/2) / cameras[myCamera]->f;
	p1[1] = -1 * (cameras[myCamera]->h/2 - y) / cameras[myCamera]->f;
	p1[2] = 1;
	cblas_sgemv(CblasRowMajor, CblasTrans, 3, 3, 1, cameras[myCamera]->rotation, 3, p1, 1, 0, t,1);
	t[0] = t[0]* depth;
	t[1] = t[1]* depth;
	t[2] = t[2]* depth;
	cblas_saxpy(3, 1, cameras[myCamera]->center , 1, t, 1);
	return t;
}

float returnQvalue(int camera, int myPair, float x1, float y1, float z1)
{
	float d[3];
	d[0] = x1;
	d[1] = y1;
	d[2] = z1;
	float x[3];
	float y[3];
	cblas_scopy(3, cameras[camera]->center, 1, x, 1);
	cblas_saxpy(3, -1, d, 1, x, 1);
	cblas_scopy(3, cameras[myPair]->center, 1, y, 1);
	cblas_saxpy(3, -1, d, 1, y, 1);
	float dot = x[0] * y[0] + x[1] * y[1] + x[2] * y[2];
	float xnorm = sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);
	float ynorm = sqrt(y[0] * y[0] + y[1] * y[1] + y[2] * y[2]);
	float sinAngle = sin(acos(dot / xnorm / ynorm));
	cblas_saxpy(3, -1, cameras[camera]->center, 1, d, 1);
	xnorm = sqrt(d[0] * d[0] + d[1] * d[1] + d[2] * d[2]);
	return cameras[camera]->f * sinAngle / xnorm;
}


Eigen::MatrixXf getNormal(ANNidxArray *nnIdx, ANNpointArray *dataPts, ANNpoint point, int image)
{
	float m[3];
	m[0] = 0;m[1] = 0; m[2] = 0;
	Eigen::MatrixXf Y(3, numNeighbours);
	for(int i = 0; i < numNeighbours; ++i)
	{
		m[0] += (*dataPts)[(*nnIdx)[i]][0];
		m[1] += (*dataPts)[(*nnIdx)[i]][1];
		m[2] += (*dataPts)[(*nnIdx)[i]][2];
	}
	m[0] = m[0]/numNeighbours;
	m[1] = m[1]/numNeighbours;
	m[2] = m[2]/numNeighbours;
	for(int i = 0; i < numNeighbours; ++i)
	{
		Y(0,i) = (*dataPts)[(*nnIdx)[i]][0] - m[0];
		Y(1,i) = (*dataPts)[(*nnIdx)[i]][1] - m[1];
		Y(2,i) = (*dataPts)[(*nnIdx)[i]][2] - m[2];
	}
	Eigen::JacobiSVD<Eigen::MatrixXf> svd(Y, Eigen::ComputeFullU);
	if(svd.matrixU().col(2)(0) * (cameras[image]->center[0] - point[0]) + svd.matrixU().col(2)(1) * (cameras[image]->center[1] - point[1]) + svd.matrixU().col(2)(2) * (cameras[image]->center[2] - point[2]) < 0)
	{
		return -1 * svd.matrixU().col(2);
	}
    return svd.matrixU().col(2);
}

int coordinate(int depth, int res, float left, float right, float value)
{
	if(depth < 1) return res;
	float mid = (left + right)/2;
	if(value < mid)
	{
		return coordinate(depth - 1, res, left, mid, value);
	}
	else return coordinate(depth - 1, res + exp2(depth - 1), mid, right, value);
}

void *normalsComputation(void *data)
{
	int myid = (long) data;
    Eigen::MatrixXf normal;
	ANNidxArray nnIdx = new ANNidx[numNeighbours];
	ANNdistArray dists = new ANNdist[numNeighbours];
	string a = "000" + to_string(myid);
	string b = a.substr(a.length() - 4);
	ofstream ply(saveFolder + "/result/" + "/result-" + b + ".ply");
	int perThread = octreeCounter/numberOfThreads;
	int first = myid * perThread;
	int last = min((myid + 1) * perThread, octreeCounter);
	int myCounter = last - first;
	ply << "ply \nformat ascii 1.0 \nelement vertex ";
    ply << myCounter;
    ply << "\nproperty float x \nproperty float y \nproperty float z \nproperty float nx \nproperty float ny \nproperty float nz \nproperty uchar red \nproperty uchar green \nproperty uchar blue \nend_header \n";
	for(int i = first; i < last; ++i)
	{
		mtxx.lock();
		kdTree->annkSearch(dataPts[i], numNeighbours, nnIdx, dists, 0);
		mtxx.unlock();
		normal = getNormal(&nnIdx, &dataPts, dataPts[i], octreeImages[i]);
		ply << dataPts[i][0]; ply << " "; ply << dataPts[i][1]; ply << " ";ply << dataPts[i][2]; ply << " ";
		ply << normal(0); ply << " "; ply << normal(1); ply << " ";ply << normal(2); ply << " ";
      	ply << octreeColors[3 * i]; ply << " ";  
      	ply << octreeColors[3 * i + 1]; ply << " ";
      	ply << octreeColors[3 * i + 2]; ply << "\n";
	}
	ply.close();
	pthread_exit(NULL);
}

void createOctree(vector<ConsistencyParameters*> *parameters, int depth)
{
	float min[3], max[3];
	min[0] = minimums[0][0];
	min[1] = minimums[0][1];
	min[2] = minimums[0][2];
	max[0] = maximums[0][0];
	max[1] = maximums[0][1];
	max[2] = maximums[0][2];
	for(int i = 1; i < (*parameters).size(); ++i)
	{
		if(minimums[i][0] < min[0]) min[0] = minimums[i][0];
		if(minimums[i][1] < min[1]) min[1] = minimums[i][1];
		if(minimums[i][2] < min[2]) min[2] = minimums[i][2];
		if(maximums[i][0] > max[0]) max[0] = maximums[i][0];
		if(maximums[i][1] > max[1]) max[1] = maximums[i][1];
		if(maximums[i][2] > max[2]) max[2] = maximums[i][2];
	}
	int treeSize = exp2(depth);
	Octree<pair<int,int> > o(treeSize, make_pair(-1,-1));
	int x, y, z;
	pair<int, int> atPosition;
	octreeCounter = 0;
	vector<Indices> indices;
	for(int i = 0; i < (*parameters).size(); ++i)
	{
		for(int j = 0; j < (*parameters)[i]->nodes.size(); ++j)
		{
			x = coordinate(depth, 0, min[0], max[0], (*parameters)[i]->nodes[j].x);
			y = coordinate(depth, 0, min[1], max[1], (*parameters)[i]->nodes[j].y);
			z = coordinate(depth, 0, min[2], max[2], (*parameters)[i]->nodes[j].z);
			atPosition = o.at(x,y,z);
			if(atPosition == o.emptyValue())
			{
				octreeCounter++;
				indices.push_back(Indices(x, y, z));
				o(x,y,z) = make_pair(i,j);
			}
			else if((*parameters)[atPosition.first]->nodes[atPosition.second].Q < (*parameters)[i]->nodes[j].Q)
			{
				o.erase(x,y,z);
				o(x,y,z) = make_pair(i,j);
			}
		}
	}
	cout << "Number of points in the final cloud: " << octreeCounter << endl;
	dataPts = annAllocPts(octreeCounter,3);
	octreeColors = (int*)malloc(3 * octreeCounter * sizeof(int));
	octreeImages = (int*)malloc(octreeCounter * sizeof(int));
	Array2D<pair <int, int> > tmp; 
	int kdTmp = 0;
	for(int i = 0; i < indices.size(); ++i)
	{
			atPosition = o.at(indices[i].x, indices[i].y, indices[i].z);
     		dataPts[kdTmp][0] = (*parameters)[atPosition.first]->nodes[atPosition.second].x;
			dataPts[kdTmp][1] = (*parameters)[atPosition.first]->nodes[atPosition.second].y;
			dataPts[kdTmp][2] = (*parameters)[atPosition.first]->nodes[atPosition.second].z;
			octreeColors[3 * kdTmp] = (*parameters)[atPosition.first]->nodes[atPosition.second].R;
			octreeColors[3 * kdTmp + 1] = (*parameters)[atPosition.first]->nodes[atPosition.second].G;
			octreeColors[3 * kdTmp + 2] = (*parameters)[atPosition.first]->nodes[atPosition.second].B;
			octreeImages[kdTmp] = (*parameters)[atPosition.first]->nodes[atPosition.second].img;
			kdTmp++;		
	}
    cout << "Computing normals" << endl;
    for(int i = 0; i < (*parameters).size(); ++i)
    {
    	(*parameters)[i]->nodes.clear();
    }
    indices.clear();
    kdTree = new ANNkd_tree(dataPts, octreeCounter, 3); 
    o.~Octree();
	pthread_t *threads = (pthread_t*)malloc(numberOfThreads * sizeof(pthread_t));
	int rc;
 	for(long i = 0; i < numberOfThreads; ++i)
  	{
  		rc = pthread_create(&threads[i], NULL, normalsComputation, (void *)i);
  	}
  	void *status;
  	for(int i = 0; i < numberOfThreads; ++i)
  	{
    	rc = pthread_join(threads[i], &status);
      	if (rc)
      	{
        	cout << "Error:unable to join thread " << rc << endl;
        	exit(-1);
      	}
  	}
}

float** loadDepthMap(string folder, int image, int currPair)
{
	ifstream file(folder + "/depth_maps/" + to_string(image) + "_" + to_string(currPair) + ".sth", ios::out | ios::binary);
	if(!file)
	{
		//cout << "Depth map " << image << " doesn't exist" << endl;
		return NULL;
	}
	float **DM = (float**)malloc(cameras[image]->w * sizeof(float*));               
    for(int i = 0; i < cameras[image]->w; ++i)
    {
    	DM[i] = (float*)malloc(cameras[image]->h * sizeof(float));
    	for(int j = 0; j < cameras[image]->h; j++) file.read((char*) &DM[i][j], sizeof(float));
    }
	file.close();
    return DM;
}


float depthRespectToCamera(int myCamera, int otherCamera, int x, int y, float depth, int *ix, int *iy, float *a, float *b, float *c)
{
	float p1[3];
	float *t = pointIn3DSpace(myCamera, x, y, depth);
	*a = t[0]; *b = t[1]; *c = t[2];
	cblas_scopy(3, cameras[otherCamera]->translation, 1, p1, 1);
	cblas_sgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1, cameras[otherCamera]->rotation, 3, t, 1, 1, p1,1);
	*ix = round((-1 * p1[0]/p1[2]) * cameras[otherCamera]->f) + cameras[otherCamera]->w/2;                                                                                                                                                                                                                                                                                                                                                                                                         
	*iy = cameras[otherCamera]->h/2 - round((-1 * p1[1]/p1[2]) * cameras[otherCamera]->f);
	free(t);
	return p1[2];
}

int getNumberOfClosestPairs(int image, int numberOfPairs)
{
	int imagesToCheck = numberOfPairs - 1;
	for(int i = 0; i < cameras[image]->consistencyCheck.size(); ++i)
	{
		imagesToCheck += cameras[cameras[image]->consistencyCheck[i].first]->numberOfPairedImages;
	}
	return imagesToCheck;
}


bool measureConsistency(int image, ConsistencyParameters *p, int tid)
{
	int numberOfPairs = cameras[image]->numberOfPairedImages;
	int currPair = 0;
	p->image = image;
	int imagesToCheck = getNumberOfClosestPairs(image, numberOfPairs);
	if(imagesToCheck > p->max) imagesToCheck = p->max;
	int C = ceil(p->ratio * imagesToCheck);
	if(imagesToCheck < C || cameras[image]->excluded)
	{
		cout << "Excluding " << image <<endl;
		return false;
	}
	while(currPair < numberOfPairs)
	{
		pthread_mutex_lock(&mutexArray[image]);
		int width = cameras[image]->w;
		int height = cameras[image]->h;
		p->myMap = loadDepthMap(saveFolder, image, cameras[image]->matchable[currPair].first);
		pthread_mutex_unlock(&mutexArray[image]);
		if(p->myMap == NULL)
		{
			currPair++;
			continue;
		}
		int itc = imagesToCheck;
		//load all other depth maps estimaed from this image and other image pairs
		for(int i = 0; i < numberOfPairs; ++i)
		{
			if(cameras[image]->matchable[currPair].first != cameras[image]->matchable[i].first)
			{
				pthread_mutex_lock(&mutexArray[image]);
				float **temp_map = loadDepthMap(saveFolder, image, cameras[image]->matchable[i].first);
				pthread_mutex_unlock(&mutexArray[image]);
				if(temp_map != NULL)
				{
					itc--;
					p->others.push_back(temp_map);
					p->othersInd.push_back(image);
					if(itc == 0)
					{
						break;
					}
				}
			}
		}
		int i = 0;
		while(itc > 0 && i < cameras[image]->consistencyCheck.size())
		{
			for(int j = 0; j < cameras[cameras[image]->consistencyCheck[i].first]->numberOfPairedImages; ++j)
			{
				if(cameras[cameras[image]->consistencyCheck[i].first]->excluded) break;
				pthread_mutex_lock(&mutexArray[cameras[image]->consistencyCheck[i].first]);
				float **temp_map = loadDepthMap(saveFolder, cameras[image]->consistencyCheck[i].first, cameras[cameras[image]->consistencyCheck[i].first]->matchable[j].first);
				pthread_mutex_unlock(&mutexArray[cameras[image]->consistencyCheck[i].first]);
				if(temp_map != NULL)
				{
					p->others.push_back(temp_map);
					p->othersInd.push_back(cameras[image]->consistencyCheck[i].first);
					itc--;
					if(itc == 0)
					{
						break;
					}
				}
			}
			i++;
		}
		int ix, iy, c;
		float dix, value, x, y, z;
		p->image0 = imread(images[image], CV_LOAD_IMAGE_COLOR);
		for(int i = 0; i < width; ++i)
			for(int j = 0; j < height; ++j)
			{
				if(p->myMap[i][j] < NANA - 1)
				{
					c = 0;
					for(int k = 0; k < p->others.size(); ++k)
					{
						dix = depthRespectToCamera(image, p->othersInd[k], i, j, p->myMap[i][j], &ix, &iy, &x, &y, &z);
						if(ix >= 0 && ix < cameras[p->othersInd[k]]->w && iy >= 0 && iy < cameras[p->othersInd[k]]->h && p->others[k][ix][iy] < NANA - 1)
						{
							value = abs(p->others[k][ix][iy] - dix)/abs(p->others[k][ix][iy]);
							if(value < cameras[p->othersInd[k]]->myEpsilon)
							{
								c++;
							}
						}
					}
					if(c >= C)
					{	
						p->nodes.push_back(OctreeNode(x, y, z, returnQvalue(image, currPair, x, y, z), (int)(p->image0).at<Vec3b>(cv::Point(i, j)).val[2], (int)(p->image0).at<Vec3b>(cv::Point(i,j)).val[1], (int)(p->image0).at<Vec3b>(cv::Point(i,j)).val[0], image));
						if(x < minimums[tid][0]) minimums[tid][0] = x;
						if(y < minimums[tid][1]) minimums[tid][1] = y;
						if(z < minimums[tid][2]) minimums[tid][2] = z;
						if(x > maximums[tid][0]) maximums[tid][0] = x;
						if(y > maximums[tid][1]) maximums[tid][1] = y;
						if(z > maximums[tid][2]) maximums[tid][2] = z;
					}
				}
			}
		p->image0.release();
		p->freeMemory(cameras[image]->w);
		currPair++;
	}
	return true;
}