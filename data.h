#include <set>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <utility>
#include <iostream>
#include <map>
#include <algorithm>
#include <mutex>
#include <ctime>
#include <cblas.h>
#include <pthread.h>
#include <time.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//global variables
//loading bundler



using namespace std;

#define PI 3.14159265
#define NANA 1000000
mutex mtxx;
int currentImage;
int currentRow;
int currentCol;

/*
Algorithm parameters
*/
string saveFolder;
string bundlerFile;
string listImages;
int pairMethod;
int minAngle, maxAngle;
float minScale, maxScale;
float daisyRad;
int daisyRadq;
int daisyThq;
int daisyHistq;
int numberOfPoints;
int gridUnitSize;
int threadsWidth;
int threadsHeight;
float probabilitySigma;
float probabilityRatio;
float probabilityRatioSparse;
float depthThreshold;
float depthSigma;
int samplingStepSparse;
int samplingStep;
int numImagesToCheck;
float imagesFactor;
float consistencyThreshold;
int numberOfThreads;
int minNumberOfImages;
int octreeDepth;
int numNeighbours;
bool onlyConsistency;
bool saveFirstCloud;
bool useConsistencyThreshold;



void loadParameters(char* fileName)
{
	ifstream file(fileName);
	float scaleDiff;
	int pointSq;
	if(file)
	{
		getline(file, saveFolder);
		getline(file, bundlerFile);
		getline(file, listImages);
		file >> pairMethod;
		file >> minAngle;
		file >> maxAngle;
		file >> scaleDiff;
		file >> daisyRad;
		file >> daisyRadq;
		file >> daisyThq;
		file >> daisyHistq;
		file >> pointSq;
		file >> gridUnitSize;
		file >> probabilitySigma;
		file >> probabilityRatio;
		file >> probabilityRatioSparse;
		file >> depthThreshold;
		file >> depthSigma;
		file >> samplingStepSparse;
		file >> numImagesToCheck;
		file >> numberOfThreads;
		file >> minNumberOfImages;
		file >> octreeDepth;
		file >> numNeighbours;
		file >> onlyConsistency;
		file >> saveFirstCloud;
		file >> useConsistencyThreshold;
		if(useConsistencyThreshold)
		{
			file >> consistencyThreshold;
		}
		minScale = 1 - scaleDiff;
		maxScale = 1 + scaleDiff;
		numberOfPoints = sqrt(pointSq);
		imagesFactor = (float)minNumberOfImages/(float)numImagesToCheck;
		samplingStep = 1;
		threadsWidth = 10;  //each thread takes 1/10 of cells per width
		threadsHeight = 10; //each thread takes 1/10 of cells per height 
		cout << "LIST OF OPTIONS:" << endl;
		cout << "Save folder: " << saveFolder << endl;
		cout << "Bundler file: " << bundlerFile << endl;
		cout << "Image list: " << listImages << endl;
		cout << "Method: " << pairMethod << endl;
		cout << "Min angle: " << minAngle << endl;
		cout << "Max angle: " << maxAngle << endl;
		cout << "Min scale: " << minScale << endl;
		cout << "Max scale: " << maxScale << endl;
		cout << "Daisy radius: " << daisyRad << endl;
		cout << "Number of daisy rings: " << daisyRadq << endl;
		cout << "Number of histograms per ring: " << daisyThq << endl;
		cout << "Number of bins in each histogram: " << daisyHistq << endl;
		cout << "Number of points per grid unit: " << pointSq << endl;
		cout << "Grid unit size: " << gridUnitSize << " x "<< gridUnitSize << " px" <<endl;
		cout << "Probability sigma: " << probabilitySigma << endl;
		cout << "Probability ratio: " << probabilityRatio << endl;
		cout << "Probability ratio sparse: " << probabilityRatioSparse << endl;
		cout << "Depth threshold: " << depthThreshold << endl;
		cout << "Depth #sigmas: " << depthSigma << endl;
		cout << "Sampling step sparse: " << samplingStepSparse << endl; 
		cout << "Number of images to check: " << numImagesToCheck << endl;
		cout << "Number of threads: " << numberOfThreads << endl;
		cout << "Minimum number of images: " << minNumberOfImages << endl;
		cout << "Octree depth: " << octreeDepth << endl;
		cout << "Number of neighbors for normals: " << numNeighbours << endl;
		cout << "Do only consistency step: " << onlyConsistency << endl;
		cout << "Save first clouds: " << saveFirstCloud << endl;
		cout << "Use user defined consistency threshold: " << useConsistencyThreshold << endl;
		if(useConsistencyThreshold)
		{
			cout << "Defined threshold: " << consistencyThreshold << endl;
		}
		cout << "-----------------------------------" << endl;
	}
	else
	{
		cout << "Options file doesn't exist" << endl;
		exit(-1);
	}
}



bool pairCompare(const std::pair<int, float>& el1, const std::pair<int, float>& el2) 
{
	return el1.second < el2.second;
}

namespace emvs
{
	class Point3D
	{
	public:
		float position[3];

		Point3D() {}

		Point3D(vector<float> r)
		{
			position[0] = r[0];
			position[1] = r[1];
			position[2] = r[2];
		}

		Point3D(float a, float b, float c)
		{
			position[0] = a;
			position[1] = b;
			position[2] = c;
		}

		~Point3D() {}
	};

}
class Camera
{
public:
	bool excluded;
	float f;
	int index;
	int numberOfPairedImages;
	//rows
	float rotation[9];
	float translation[3];
	float center[3];
	vector<pair<int, float> > matchable;
	vector<pair<int, float> > consistencyCheck;
	//points seen on camera
	set<int> points;
	int bestPair;
	map<int, float> scaleRatio;
	int w, h;
	float myEpsilon;

	Camera() { }

	void calculateDepthDiscretizationError()
	{
		float result = max(w,h);
		myEpsilon = abs((result/2)/((result/2) + 0.5) - 1);
	}

	void calculate_center()
	{
		cblas_sgemv(CblasRowMajor, CblasTrans, 3, 3, -1, this->rotation, 3, this->translation, 1, 0, this->center,1);
	}

	void sortPairs()
	{
		sort(this->matchable.begin(), this->matchable.end(), pairCompare);
		calculateDepthDiscretizationError();
		numberOfPairedImages = 1;
		if(this->matchable.size() > 0)
		{
			this->points.clear();
			sort(this->consistencyCheck.begin(), this->consistencyCheck.end(), pairCompare);
		}
		else
		{
			this->excluded = true;
		} 
	}

};

//global variables
vector<Camera* > cameras;
vector<emvs::Point3D* > points;
vector<string> images;




void load_bundler(string fileName)
{
	ifstream file(fileName);
	if(!file)
	{
		cout << "Bundler file doesn't exist" << endl;
		exit(-1);
	}
	string line;
	getline(file,line);
	int numCameras, numPoints, temp1, camera;
	float temp;
	file >> numCameras;
	file >> numPoints;
	cout << "Reading cameras and points: " << numCameras << " " << numPoints << endl;
	for(int i = 0; i < numCameras; i++)
	{
		Camera* c = new Camera();
		c->index = i;
		file >> temp;
		c->f = temp;
		if(c->f < 0.0000001)
		{
			c->excluded = true;
		}
		else c->excluded = false;
		file >> temp;
		file >> temp;
		for(int j = 0; j < 9; j++)
		{
			file >> c->rotation[j];

		}
		for(int j = 0; j < 3; j++)
		{
			file >> c->translation[j];

		}
		c->calculate_center();
		cameras.push_back(c);
	}
	for(int i = 0; i < numPoints; i++)
	{
		emvs::Point3D *P = new emvs::Point3D();
		file >> P->position[0];
		file >> P->position[1];
		file >> P->position[2];
		points.push_back(P); 
		file >> temp1;
		file >> temp1;
		file >> temp1;
		file >> temp1;
		for(int j = 0; j < temp1; j++)
		{
			file >> camera;
			cameras[camera]->points.insert(i);
			file >> camera;
			file >> temp;
			file >> temp;

		}
	}
	file.close();
}

void load_images(string fileName, int number)
{
	ifstream file(fileName);
	if(!file)
	{
		cout << "Images file doesn't exist" << endl;
		exit(-1);
	}
	string line;
	cv::Mat image;
	for(int i = 0; i < number; i++)
	{
		getline(file,line);
		images.push_back(line);
		image = cv::imread(line , CV_LOAD_IMAGE_GRAYSCALE);
		cameras[i]->h = image.rows;
		cameras[i]->w = image.cols;
		image.release();
	}
	file.close();
}

class OctreeNode
{
public:
	float x, y, z, Q;
	int R, G, B;
	int img;

	//ovako pohranjujemo element octree strukture
	OctreeNode(float _x, float _y, float _z, float q, int r, int g, int b, int _img)
	{
		x = _x;
		y = _y;
		z = _z;
		Q = q;
		R = r;
		G = g;
		B = b;
		img = _img;
	}
};

class ConsistencyParameters
{
public:
	int image, max, threads, minNumImages;
	float ratio, epsilon;
	vector<float**> others;
	vector<int> othersInd;
	vector<OctreeNode> nodes;
	float **myMap;
	cv::Mat image0;
	vector<emvs::Point3D*> finalCloud;
	vector<pair<int,int> > coordinates;

	ConsistencyParameters(int img, int ma, float r, float e, int t, int minImages)
	{
		image = img;
		max = ma;
		ratio = r;
		epsilon = e;
		threads = t;
		minNumImages = minImages;
	}

	void freeMemory(int w)
	{
		for(int i = 0; i < w; ++i)
		{
			free(myMap[i]);
		}
		free(myMap);
		for(int i = 0; i < others.size(); ++i)
		{
			w = cameras[othersInd[i]]->w;
			for(int j = 0; j < w; ++j)
			{
				free(others[i][j]);
			}
			free(others[i]);
		}
		others.clear();
		othersInd.clear();
	}

};


struct UniformValues
{
   double depth;
   double value;
   int x,y,index;

   UniformValues(double d, int X, int Y, double v, int i)
   {
      depth = d;
      x = X;
      y = Y;
      value = v;
      index = i;
   }
};

struct ForDaisy
{
	int radq, thq, histq;
	float rad;
	string imageName;

	ForDaisy(float r, int rq, int tq, int hq, string im)
	{
		rad = r;
		radq = rq;
		thq = tq;
		histq = hq;
		imageName = im;
	}
};

bool compareF(UniformValues &el1, UniformValues &el2) {
  return el1.value < el2.value;
}

class Indices
{

public:
	int x, y, z;

	Indices(int _x, int _y, int _z)
	{
		x =_x;
		y = _y;
		z = _z;
	}
};


