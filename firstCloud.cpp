#include "imagePairs.cpp"
#include <daisy/daisy.h>
#include <algorithm>
#include <fstream>
#include <cstdio>
#include <map>
#include <ANN/ANN.h>



using namespace cv;
using namespace std;
using namespace kutility;

/*
Precalculated descriptors
*/

float *daisy0;
float *daisy1;
daisy* daisy00;
daisy* daisy10;
float **depthMap;
int descriptorSize;

float* getDescriptorCopy(int y, int x, int w, int size, int daisyIndex)
{
	float *d = new float[size];
	if(daisyIndex == 0) for(int i = 0; i < size; ++i) d[i] = daisy0[(y * w + x) * size + i];
	else for(int i = 0; i < size; ++i) d[i] = daisy1[(y * w + x) * size + i];
	return d;
}

float* getDescriptor(int y, int x, int w, int size, int daisyIndex)
{
	if(daisyIndex == 0) return &daisy0[(y * w + x) * size];
	else return &daisy1[(y * w + x) * size];
}

void getCoordinatesOnTheOtherImage(int x, int y, int *cx, int *cy, float depth, int w0, int h0, int image, int bestPair)
{
	  float p1[3];
    float t[3];
    p1[0] = -1 * (x - w0/2) / cameras[image]->f;
    p1[1] = -1 * (h0/2 - y) / cameras[image]->f;
    p1[2] = 1;
    cblas_sgemv(CblasRowMajor, CblasTrans, 3, 3, 1, cameras[image]->rotation, 3, p1, 1, 0, t,1);
    t[0] = t[0]* depth;
    t[1] = t[1]* depth;
    t[2] = t[2]* depth;
    cblas_saxpy(3, 1, cameras[image]->center , 1, t, 1);
    cblas_scopy(3, cameras[bestPair]->translation, 1, p1, 1);
    cblas_sgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1, cameras[bestPair]->rotation, 3, t, 1, 1, p1,1);
    *cx = round((-1 * p1[0]/p1[2]) * cameras[bestPair]->f);
    *cy = round((-1 * p1[1]/p1[2]) * cameras[bestPair]->f);
}

bool depthsCalculation(double* depthMin, double* depthMax, float *a, float *b, int w1, int h1)
{
	  bool side;
	  vector<float> solution;
	  float t = (-2 * b[0] - w1 * b[2])/(2 * a[0] + w1 * a[2]);
	  int check = round((t * a[1] + b[1])/(t * a[2] + b[2])); 
	  if(check > -1*h1/2 && check < h1/2)
	  {
	     solution.push_back(t);
	  }
	  t = (-2 * b[1] - h1 * b[2])/(2 * a[1] + h1 * a[2]);
	  check = round((t * a[0] + b[0])/(t * a[2] + b[2]));
	  if(check > -1*w1/2 && check < w1/2)
	  {
	     solution.push_back(t);
	  }
	  if(solution.size() == 2)
	  {
	     if(solution[0] < solution[1])
	     {
	        *depthMin = solution[0];
	        *depthMax = solution[1];
	     }
	     else
	     {
	        *depthMin = solution[1];
	        *depthMax = solution[0];
	     }
	     return true;
	  }
	  t = (-2 * b[1] + h1 * b[2])/(2 * a[1] - h1 *a[2]);
	  check = round((t * a[0] + b[0])/(t * a[2] + b[2])); 
	  if(check > -1*w1/2 && check < w1/2)
	  {
	     solution.push_back(t);
	  }
	  if(solution.size() == 2)
	  {
	     if(solution[0] < solution[1])
	     {
	        *depthMin = solution[0];
	        *depthMax = solution[1];
	     }
	     else
	     {
	        *depthMin = solution[1];
	        *depthMax = solution[0];
	     }
	     return true;
	  }  
	  t = (-2 * b[0] + w1 * b[2])/(2 * a[0] - w1 * a[2]);
	  check = round((t * a[1] + b[1])/(t * a[2] + b[2])); 
	  if(check > -1*h1/2 && check < h1/2)
	  {
	     solution.push_back(t);
	  }
	  if(solution.size() < 2) return false;
	  if(solution[0] < solution[1])
	  {
	     *depthMin = solution[0];
	     *depthMax = solution[1];
	  }
	  else
	  {
	     *depthMin = solution[1];
	     *depthMax = solution[0];
	  }  
	  return true;
}


int getRotation(int image, int bestPair, int w0, int h0, int w1, int h1)
{
    int x = w0 / 2;
    int y = h0 / 2;
    float point[3];
    float a[3];
    float b[3];
    float t[3];
    float daisyOrientation = 0;
    int orientation;
    point[0] = x - w0/2;
    point[1] = h0/2 - y;
    point[2] = 1;
    a[0] = (-1.0 * point[0]) / cameras[image]->f;
    a[1] = (-1.0 * point[1]) / cameras[image]->f;
    a[2] = 1;
    cblas_sgemv(CblasRowMajor, CblasTrans, 3, 3, 1, cameras[image]->rotation, 3, a, 1, 0, t,1);
    cblas_sgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1, cameras[bestPair]->rotation, 3, t, 1, 0, a,1);
    a[0] = -1.0 * a[0] * cameras[bestPair]->f;
    a[1] = -1.0 * a[1] * cameras[bestPair]->f;
    cblas_scopy(3, cameras[image]->center, 1, t, 1);
    cblas_saxpy(3, -1, cameras[bestPair]->center, 1, t, 1);
    cblas_sgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1, cameras[bestPair]->rotation, 3, t, 1, 0, b,1);
    b[0] = -1.0 * b[0] * cameras[bestPair]->f;
    b[1] = -1.0 * b[1] * cameras[bestPair]->f;
    double depthMin, depthMax;
    depthsCalculation(&depthMin, &depthMax, a, b, w1, h1); 
    int cx, cy;
    getCoordinatesOnTheOtherImage(x - 100, y, &cx, &cy, (depthMax + depthMin)/2, w0, h0, image, bestPair); 
    int cxx, cyy;  
    getCoordinatesOnTheOtherImage(x + 100, y, &cxx, &cyy, (depthMax + depthMin)/2, w0, h0, image, bestPair); 
    if((cxx - cx) > 0) daisyOrientation = -1 * atan(float(cyy - cy)/(cxx - cx)) * 180.0 / PI;
    else daisyOrientation = -1 * atan(float(cyy - cy)/(cxx - cx)) * 180.0 / PI - 180;
    if(daisyOrientation < 0) daisyOrientation += 360;
    if(daisyOrientation != daisyOrientation) daisyOrientation = 0;
    orientation = round(daisyOrientation);
    if(orientation == 360) return 0;
    return orientation;
}

void *calculateDaisy(void* parameters)
{
  ForDaisy* d = (ForDaisy*)parameters;
  uchar *im0 = NULL;
  Mat image0 = imread(d->imageName , CV_LOAD_IMAGE_GRAYSCALE);
  imwrite(saveFolder + "/temp/0.pgm", image0);
  image0.release();
  int w,h;
  load_gray_image(saveFolder + "/temp/0.pgm", im0, h, w);
  daisy00 = new daisy();
  daisy00->verbose(0);
  daisy00->set_image(im0, h, w);
  daisy00->set_parameters(d->rad, d->radq, d->thq, d->histq);
  daisy00->disable_interpolation();
  daisy00->initialize_single_descriptor_mode();
  daisy00->set_normalization(NRM_FULL);
  descriptorSize = daisy00->descriptor_size();
  daisy00->compute_descriptors();
  daisy00->normalize_descriptors();
  daisy0 = daisy00->get_descriptor_array();
  depthMap = (float**)malloc(w *  sizeof(float*));
  for(int i = 0; i < w; ++i)
  {
    depthMap[i] = (float*)malloc(h * sizeof(float));
    fill(depthMap[i], depthMap[i] + h, NANA);
  }
  pthread_exit(NULL);
}

void prepareImagePair(int image, float rad, int radq, int thq, int histq)
{
    pthread_t thread1;
    int rc = pthread_create(&thread1, NULL, calculateDaisy, (void *)new ForDaisy(rad, radq, thq, histq, images[image]));
    if (rc)
    {
      cout << "Error:unable to join," << rc << endl;
      exit(-1);
    }uchar *im1 = NULL;
    Mat image1 = imread(images[cameras[image]->bestPair], CV_LOAD_IMAGE_GRAYSCALE);
    int bestPair = cameras[image]->bestPair;
    imwrite(saveFolder + "/temp/1.pgm", image1);
    image1.release();
    int w1, h1;
    load_gray_image(saveFolder + "/temp/1.pgm", im1, h1, w1);
    float radi = rad / cameras[image]->scaleRatio[bestPair];
    daisy10 = new daisy();
    daisy10->verbose(0);
    daisy10->set_image(im1, h1, w1);
    daisy10->set_parameters(radi, radq, thq, histq);
    daisy10->disable_interpolation(); 
    daisy10->initialize_single_descriptor_mode();
    daisy10->set_normalization(NRM_FULL);
    int orientation = getRotation(image, bestPair, cameras[image]->w, cameras[image]->h, w1, h1);
    daisy10->compute_descriptors_ori(orientation);
    daisy10->normalize_descriptors();
    daisy1 = daisy10->get_descriptor_array();
    void *status;
    rc = pthread_join(thread1, &status);
    if (rc)
    {
      cout << "Error:unable to join," << rc << endl;
       exit(-1);
    }
}

void freeMemory(int w0)
{
    daisy00->~daisy();
    daisy10->~daisy();
    for (int i = 0; i < w0; i++) free(depthMap[i]);
}


float* return3dPoint(float depth, int x, int y, int w0, int h0, int image)
{
   float p1[3];
   float* t = (float*)malloc(3 * sizeof(float));
   p1[0] = -1 * ((float)x - (float)w0/2) / cameras[image]->f;
   p1[1] = -1 * ((float)h0/2 - (float)y) / cameras[image]->f;
   p1[2] = 1.0;
   cblas_sgemv(CblasRowMajor, CblasTrans, 3, 3, 1, cameras[image]->rotation, 3, p1, 1, 0, t,1);
   t[0] = t[0]* depth;
   t[1] = t[1]* depth;
   t[2] = t[2]* depth;
   cblas_saxpy(3, 1, cameras[image]->center , 1, t, 1);
   return t;
}

void saveToPLY(string folder, int image, int counter, int w0, int h0)
{
   int contSaved = 0;
   cv::Mat image0 = cv::imread(images[image], CV_LOAD_IMAGE_COLOR);
   ofstream ply(folder + "/" + to_string(image) + "_" + to_string(cameras[image]->bestPair) + ".ply");
   float* r;
   ply << "ply \n format ascii 1.0 \n element vertex ";
   ply << counter;
   ply << "\n property float x \n property float y \n property float z \n property uchar red \n property uchar green \n property uchar blue \n end_header \n";
    for(int i = 0; i < w0; ++i)  
      for(int j = 0; j < h0; ++j)
      {
         if(depthMap[i][j] < NANA - 1)
         {
            r = return3dPoint(depthMap[i][j], i, j, w0, h0, image);
            ply << r[0]; ply << " "; ply << r[1]; ply << " ";ply << r[2]; ply << " ";
            ply << (int)image0.at<Vec3b>(cv::Point(i, j)).val[2]; ply << " ";  
            ply << (int)image0.at<Vec3b>(cv::Point(i, j)).val[1]; ply << " ";
            ply << (int)image0.at<Vec3b>(cv::Point(i, j)).val[0]; ply << "\n";
            contSaved++;
            free(r);
         }
      }
   image0.release();
   ply.close();
}


void saveMatrixToBinary(string folder, int image, int w0, int h0)
{
  ofstream fout(folder + "/depth_maps/" + to_string(image) + "_" + to_string(cameras[image]->bestPair) + ".sth", ios::binary);
  int ca = 0;
  for (int i = 0; i < w0; i++)
    for (int j = 0; j < h0; j++)
    {
      if(depthMap[i][j] < NANA - 1)
      {
      	ca++;
      } 
    	fout.write ((char*)&depthMap[i][j], sizeof(float));
    }
    cout << image << " & " << cameras[image]->bestPair <<" --> size: "<< ca;
  	if(saveFirstCloud) saveToPLY(saveFolder + "/depth_maps", image, ca, cameras[image]->w, cameras[image]->h);
  	freeMemory(w0);
}




class Information
{
   public:
      int image;
      int bestPair;
      int size;
      int w0, h0, w1, h1, maxDimension;
      int widthStep, heightStep;
      int divideWidth, divideHeight;
      int numberOfPoints;
      float ratio, sparseRatio;
      float sigma;
      int radius;
      int id, row, col; 
      int numRows, numCols;
      int counter;
      map<pair<int, int> , float> gridDepths;

      vector<UniformValues>::iterator result;
      vector<UniformValues> v;



      Information(int i, int _image, int r, int c, int nr, int nc, float rad, int _numberOfPoints, int _gridUnitSize, float _sigma, float _ratio, float _sparseRatio, int _size, int _w0, int _h0, int _w1, int _h1)
      {
         counter = 0;
      	 w0 = _w0;
         h0 = _h0;
         w1 = _w1;
         h1 = _h1;
         widthStep = _gridUnitSize;
         heightStep = _gridUnitSize;
         divideWidth = w0 / widthStep;  
         if(w0 % widthStep != 0) divideWidth++;     
      	 divideHeight = h0 / heightStep;
      	 if(h0 % heightStep != 0) divideHeight++;
         ratio = _ratio;
         sparseRatio = _sparseRatio;
         radius = rad;
         sigma = _sigma;
         image = _image;
         numberOfPoints = _numberOfPoints;
         size = _size;
         id = i;
         row = r;
         col = c;
         numRows = nr;
         numCols = nc;
         bestPair = cameras[image]->bestPair;
         if(w0 > h0) maxDimension = w0;
         else maxDimension = h0;
      }

      bool nonUniformSpaceSampling(int x, int y, int samplingResolution, double depthMin, double depthMax, bool calculateDepths, double *sumValues)
      {
         float point[3];
         float a[3];
         float b[3];
         float t[3];
         point[0] = x - this->w0/2;
         point[1] = this->h0/2 - y;
         point[2] = 1;
         a[0] = (-1.0 * point[0]) / cameras[this->image]->f;
         a[1] = (-1.0 * point[1]) / cameras[this->image]->f;
         a[2] = 1;
         cblas_sgemv(CblasRowMajor, CblasTrans, 3, 3, 1, cameras[this->image]->rotation, 3, a, 1, 0, t,1);
         cblas_sgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1, cameras[this->bestPair]->rotation, 3, t, 1, 0, a,1);
         a[0] = -1.0 * a[0] * cameras[this->bestPair]->f;
         a[1] = -1.0 * a[1] * cameras[this->bestPair]->f;
         cblas_scopy(3, cameras[this->image]->center, 1, t, 1);
         cblas_saxpy(3, -1, cameras[this->bestPair]->center, 1, t, 1);
         cblas_sgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1, cameras[this->bestPair]->rotation, 3, t, 1, 0, b,1);
         b[0] = -1.0 * b[0] * cameras[this->bestPair]->f;
         b[1] = -1.0 * b[1] * cameras[this->bestPair]->f;
         if(calculateDepths)
         {
            if(!depthsCalculation(&depthMin, &depthMax, a, b, this->w1, this->h1)) return false;
         }
         if(depthMax > 0) depthMax = -1;
         if(depthMin > 0) depthMin = -1;
         double uMin = (depthMin * a[0] + b[0])/(depthMin * a[2] + b[2]);
         double vMin = (depthMin * a[1] + b[1])/(depthMin * a[2] + b[2]);
         double uMax = (depthMax * a[0] + b[0])/(depthMax * a[2] + b[2]);
         double vMax = (depthMax * a[1] + b[1])/(depthMax * a[2] + b[2]);
         double dl = sqrt((uMax - uMin)*(uMax - uMin) + (vMax - vMin)*(vMax - vMin));
         if (dl > depthThreshold * this->maxDimension && calculateDepths == false)
         {
            return false;
         }
         if (dl == 0) return false;
         double du = (uMax - uMin)/dl;
         double dv = (vMax - vMin)/dl;
         double omegaMin = depthMin * a[2] + b[2];
         double omega = omegaMin;
         double depth = depthMin;
         double u = uMin;
         double v = vMin;
         double dDepth;
         double orientation;
         bool in = false;
         int uInt, vInt;
         float* d0 = getDescriptor(y, x, this->w0, this->size, 0);
         int index = 0;
         while(depth < depthMax)
         { 
            uInt = round(u) + this->w1 / 2;
            vInt = this->h1 / 2 - round(v);
            if(uInt >= 0 && uInt < this->w1 && vInt >= 0 && vInt < this->h1)
            {
               in = true;
               float *d1 = getDescriptorCopy(vInt, uInt, this->w1, this->size, 1);
               cblas_saxpy(this->size, -1, d0, 1, d1, 1);
               double norm1 = 0;
               for(int ii = 0; ii < this->size; ++ii) norm1 += d1[ii] * d1[ii];
               double norm = exp(-1 * norm1 / this->sigma);
               if(norm != norm)
               {
                  this->v.push_back(UniformValues(0, uInt, vInt, 0, index));
               }
               else
               {
                  this->v.push_back(UniformValues(depth, uInt, vInt, norm, index));
                  *sumValues +=norm;
               }
               index++;
               free(d1);
            }
            else if(in) break;
            if(du != 0) dDepth = (omega * samplingResolution * du)/(a[0] - a[2]*(u + samplingResolution*du));
            else dDepth = (omega * samplingResolution * dv)/(a[1] - a[2]*(v + samplingResolution*dv));
            depth += dDepth;
            u += samplingResolution * du;
            v += samplingResolution * dv;
            omega = depth * a[2] + b[2];
         }
         return true;
   }

   vector<UniformValues>::iterator computeProbabilityDistribution(vector<UniformValues> *values, bool sparse, int dev)
   {
         if((*values).size() == 0) return (*values).end();
         vector<UniformValues>::iterator first, s1, s2;
         s1 = (*values).end();
         s2 = (*values).end();
         float r = this->ratio;
         if(sparse) r = this->sparseRatio;
         int radi = this->radius / dev;
         first = max_element((*values).begin(),(*values).end(),compareF);
         if(first->value == 0) return (*values).end();;
         if((*first).index - radi - 1 >= 0)
         {
            s1 = max_element((*values).begin(), first - radi,compareF);
         }
         if((*first).index + radi + 1 < (*values).size())
         {
            s2 = max_element(first + radi + 1, (*values).end() ,compareF);
         }
         if(s1 == (*values).end() && s2 == (*values).end())
         {
            return first;
         }
         if(s1 == (*values).end() && s2 != (*values).end())
         {
            s1 = s2;
         }
         if(s1 != (*values).end() && s2 != (*values).end())
         {
            if((*s2).value > (*s1).value) s1 = s2;
         }
         if(((*s1).value / (*first).value) < r)
         {
            return first;

         } 
         else return (*values).end(); 
   }

   void returnDepths(int i, int j, float *dMin, float *dMax, bool *d)
   {
     int number = 0;
     float sigma = 0;
     if((this->gridDepths).count(make_pair(i,j)) <= 0)
     {
        *d = false;
        return;
     }
     if(i > 0 && i < this->divideWidth - 1 && j > 0 && j < this->divideHeight - 1)
     {
        number = 0;
        for(int m = i - 1; m <= i + 1; m++)
        {
           for(int n = j - 1; n <= j + 1; n++)
           {
              if((this->gridDepths).count(make_pair(m,n)))
              {  
                 sigma += ((this->gridDepths)[make_pair(m,n)] - (this->gridDepths)[make_pair(i,j)])*((this->gridDepths)[make_pair(m,n)] - (this->gridDepths)[make_pair(i,j)]);
                 number++;
              }
           }
        }
        if(number == 0)
        {
           *d = false;
           return;
        }
        sigma = sqrt(sigma/(number - 1));
     } 
     *dMin = (this->gridDepths)[make_pair(i,j)] - depthSigma*sigma;
     *dMax = (this->gridDepths)[make_pair(i,j)] + depthSigma*sigma;
     if(number > 1) *d = true;
  }

 };


void divide(vector<UniformValues> *values, int suma)
{
   for(int i = 0; i < values->size(); i++) (*values)[i].value = ((*values)[i].value)/suma;
}



void computeFirstCloud(Information* inf)
{
	double sumValues;
	int left, right, up, down, xrand, yrand, xstep, ystep;
   	float depthsSum;
   	int depthsCounter;
   	int perThreadw = inf->divideWidth/inf->numCols;
   	if(inf->divideWidth % inf->numCols != 0) perThreadw++;
   	int perThreadh = inf->divideHeight/inf->numRows;
   	if(inf->divideHeight % inf->numRows != 0) perThreadh++;
   	int iMin = max(perThreadw * inf->col - 1, 0);
   	int iMax = min(perThreadw * (inf->col + 1) + 1, inf->divideWidth);
   	int jMin = max(perThreadh * inf->row - 1, 0);
   	int jMax = min(perThreadh * (inf->row + 1) + 1, inf->divideHeight);
   	int counter = 0;
   	for(int i = iMin; i < iMax; ++i)
   	{
      for(int j = jMin; j < jMax; ++j)
      {
         depthsSum = 0;
         depthsCounter = 0;
         left = i * inf->widthStep;
         right = (i + 1) * inf->widthStep;
         if(i == inf->divideWidth - 1)
         {
         	right = inf->w0;
         } 
         up = j * inf->heightStep;
         down = (j + 1) * inf->heightStep;
         if(j == inf->divideHeight - 1)
         {
         	down = inf->h0;
         }
         xstep = (right - left)/inf->numberOfPoints;
         ystep = (down - up)/inf->numberOfPoints;
         for(xrand = left; xrand < right; xrand += xstep)
         {
            for(yrand = up; yrand < down; yrand += ystep)
            {
               sumValues = 0;
               if(!inf->nonUniformSpaceSampling(xrand , yrand, samplingStepSparse, 0, 0, true, &sumValues)) continue;
               divide(&(inf->v), sumValues);
               inf->result = inf->computeProbabilityDistribution(&(inf->v), true, samplingStepSparse);
               if(inf->result != (inf->v).end())
               {
               	  counter++;
               	  depthsSum += inf->result->depth;
                  depthsCounter++;
               }             
               (inf->v).clear();
            }
         }
         if(depthsCounter > 0)
         {  
          inf->gridDepths[make_pair(i,j)] = depthsSum/depthsCounter;
         }
        }
   }
   float dMin = 0; 
   float dMax = 0;
   bool d;
   iMin = perThreadw * inf->col;
   iMax = min(perThreadw * (inf->col + 1), inf->divideWidth);
   jMin = perThreadh * inf->row;
   jMax = min(perThreadh * (inf->row + 1), inf->divideHeight);
   inf->counter = 0;
   for(int i = iMin; i < iMax; i++)
   {
      for(int j = jMin; j < jMax; j++)
      {
         left = i * inf->widthStep;
         right = (i + 1) * inf->widthStep;
         if(i == inf->divideWidth - 1) right = inf->w0;
         up = j * inf->heightStep;
         down = (j + 1) * inf->heightStep;
         if(j == inf->divideHeight - 1) down = inf->h0;
         d = false;
         inf->returnDepths(i, j, &dMin, &dMax, &d);
         if(!d) continue;
         for(int n = left; n < right; n++)
         {
            for(int m = up; m < down; m++)
            {
               sumValues = 0;
               if(depthMap[n][m] > (NANA - 1))
               {
                  if(!inf->nonUniformSpaceSampling(n, m, samplingStep, dMin, dMax, false, &sumValues)) continue;
                  divide(&(inf->v), sumValues);
                  inf->result = inf->computeProbabilityDistribution(&(inf->v), false, samplingStep);
                  if(inf->result != (inf->v).end())
                  {
                     inf->counter++;
                     depthMap[n][m] = inf->result->depth;
                  }
               }
               (inf->v).clear();
            }
         }
      }
   }
}