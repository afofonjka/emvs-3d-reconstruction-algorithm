#include "data.h"
#include <iostream>
#include <vector> 
#include <cmath>
#include <algorithm>
#include <cstdlib>


using namespace std;

/*
There are 3 ways to select image pairs. All of them are implemented inside the function: findImagePairs

0 - image pair is the one that minimizes the the value = angle between cameras + angle of the streight line on the first 
    image projected on the other (descriptor works better if this angle is smaller)

1 - image pair is the one that minimizes the the angle between cameras


2 - image pair is the one that has the closest scale

*/


float scale(Camera* first, Camera* second, emvs::Point3D* p, int expected)
{
	float r = (expected * p->position[2]) / first->f;
	float middle1[3];
	float middle2[3];
	float res1, res2;
	cblas_scopy(3, first->translation, 1, middle1, 1);
	cblas_sgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1, first->rotation, 3, p->position, 1, 1, middle1,1);
	cblas_scopy(3, second->translation, 1, middle2, 1);
	cblas_sgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1, second->rotation, 3, p->position, 1, 1, middle2,1);
	res1 = (-1 * middle1[0])/middle1[2] * first->f;
	res2 = (-1 * (middle1[0] + r))/middle1[2] * first->f;
	float first1 = res1 - res2;
	res1 = (-1 * middle2[0])/middle2[2] * second->f;
	res2 = (-1 * (middle2[0] + r))/middle2[2] * second->f;
	float second1 = res1 - res2;
	return abs(first1/second1);
}

float angle(Camera* first, Camera* second, emvs::Point3D* p)
{
	float x[3];
	float y[3];
	cblas_scopy(3, first->center, 1, x, 1);
	cblas_saxpy(3, -1, p->position, 1, x, 1);
	cblas_scopy(3, second->center, 1, y, 1);
	cblas_saxpy(3, -1, p->position, 1, y, 1);
	float dot = x[0] * y[0] + x[1] * y[1] + x[2] * y[2];
	float xnorm = sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);
	float ynorm = sqrt(y[0] * y[0] + y[1] * y[1] + y[2] * y[2]);
	float angle = acos(dot / xnorm / ynorm);
	return angle * 180.0 / PI;

}

void getCoordinatesOnTheOtherImage(Camera *first, Camera *second, int x, int y, int *cx, int *cy, float depth)
{
	float p1[3];
	float t[3];
	p1[0] = -1 * (x - first->w/2) / first->f;
	p1[1] = -1 * (first->h/2 - y) / first->f;
	p1[2] = 1;
	cblas_sgemv(CblasRowMajor, CblasTrans, 3, 3, 1, first->rotation, 3, p1, 1, 0, t,1);
	t[0] = t[0]* depth;
	t[1] = t[1]* depth;
	t[2] = t[2]* depth;
	cblas_saxpy(3, 1, first->center , 1, t, 1);
	cblas_scopy(3, second->translation, 1, p1, 1);
	cblas_sgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1, second->rotation, 3, t, 1, 1, p1,1);
	*cx = round((-1 * p1[0]/p1[2]) * second->f);
	*cy = round((-1 * p1[1]/p1[2]) * second->f);
}

bool getMinAndMaxDepth(double* depthMin, double* depthMax, float *a, float *b, int w1, int h1)
{
    bool side;
	vector<float> solution;
	float t = (-2 * b[0] - w1 * b[2])/(2 * a[0] + w1 * a[2]);
	int check = round((t * a[1] + b[1])/(t * a[2] + b[2])); 
	if(check > -1*h1/2 && check < h1/2)
	{
	    solution.push_back(t);
	}
	t = (-2 * b[1] - h1 * b[2])/(2 * a[1] + h1 *a[2]);
	check = round((t * a[0] + b[0])/(t * a[2] + b[2]));  
	if(check > -1*w1/2 && check < w1/2)
	{
		solution.push_back(t);
	}
	if(solution.size() == 2)
	{
		if(solution[0] < solution[1])
	    {
	    	*depthMin = solution[0];
	    	*depthMax = solution[1];
	    }
	    else
	    {
	    	*depthMin = solution[1];
	    	*depthMax = solution[0];
	    }
	    return true;
	}
	t = (-2 * b[1] + h1 * b[2])/(2 * a[1] - h1 *a[2]);
	check = round((t * a[0] + b[0])/(t * a[2] + b[2])); 
	if(check > -1*w1/2 && check < w1/2)
	{
		solution.push_back(t);
	}
	if(solution.size() == 2)
	{
		if(solution[0] < solution[1])
		{
			*depthMin = solution[0];
			*depthMax = solution[1];
		}
		else
		{
			*depthMin = solution[1];
			*depthMax = solution[0];
		}
		return true;
	}  
	t = (-2 * b[0] + w1 * b[2])/(2 * a[0] - w1 * a[2]);
	check = round((t * a[1] + b[1])/(t * a[2] + b[2])); 
	if(check > -1*h1/2 && check < h1/2)
	{
		solution.push_back(t);
	}
	if(solution.size() < 2) return false;
	if(solution[0] < solution[1])
	{
		*depthMin = solution[0];
		*depthMax = solution[1];
	}
	else
	{
		*depthMin = solution[1];
		*depthMax = solution[0];
	}  
	return true;
}

//return distance from 0 or 360
float daisyRotation(Camera *first, Camera *second)
{
	int x = first->w / 2;
    int y = first->h / 2;
    float point[3];
    float a[3];
    float b[3];
    float t[3];
    point[0] = x - first->w/2;
    point[1] = first->h/2 - y;
    point[2] = 1;
    a[0] = (-1.0 * point[0]) / first->f;
    a[1] = (-1.0 * point[1]) / first->f;
    a[2] = 1;
    cblas_sgemv(CblasRowMajor, CblasTrans, 3, 3, 1, first->rotation, 3, a, 1, 0, t,1);
    cblas_sgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1, second->rotation, 3, t, 1, 0, a,1);
    a[0] = -1.0 * a[0] * second->f;
    a[1] = -1.0 * a[1] * second->f;
    cblas_scopy(3, first->center, 1, t, 1);
    cblas_saxpy(3, -1, second->center, 1, t, 1);
    cblas_sgemv(CblasRowMajor, CblasNoTrans, 3, 3, 1, second->rotation, 3, t, 1, 0, b,1);
    b[0] = -1.0 * b[0] * second->f;
    b[1] = -1.0 * b[1] * second->f;
    double depthMin, depthMax;
    getMinAndMaxDepth(&depthMin, &depthMax, a, b, first->w, first->h); 
    int cx, cy;
    getCoordinatesOnTheOtherImage(first, second, x - 1000, y, &cx, &cy, (depthMax + depthMin)/2); 
    int cxx, cyy;  
    getCoordinatesOnTheOtherImage(first, second, x + 1000, y, &cxx, &cyy, (depthMax + depthMin)/2); 
    float result = 0;
    if((cxx - cx) > 0) result = -1 * atan(float(cyy - cy)/(cxx - cx)) * 180.0 / PI;
    else result = -1 * atan(float(cyy - cy)/(cxx - cx)) * 180.0 / PI - 180;
    if(result < 0) result += 360;
    if(result < 180) return result;
    else return 360 - result;
}


pair<float, float> averageAngleAndScale(Camera *first, Camera *second)
{
	vector<int> common_data;
	set_intersection(first->points.begin(),first->points.end(),second->points.begin(),second->points.end(),back_inserter(common_data));
	float scaleAvg = 0;
	float angleAvg = 0;
	for(int i = 0; i < common_data.size(); i++)
	{
		scaleAvg += scale(first, second, points[common_data[i]], 20);
		angleAvg += angle(first, second, points[common_data[i]]);
	} 
	if(common_data.size() != 0)
	{
		scaleAvg = scaleAvg / common_data.size();
		angleAvg = angleAvg / common_data.size();
	}
	else
	{
		scaleAvg = 100;
		angleAvg = 100;
	}
	pair<float, float> toReturn;
	toReturn.first = angleAvg;
	toReturn.second = scaleAvg;
	return toReturn;
}

//first line number
//pair1 pair2
void changeImagePairs(string fileName)
{
	ifstream file(fileName);
	if(!file)
	{
		cout << "File doesn't exist" << endl;
		return;
	}
	int number, first, second;
	file >> number;
	while(number > 0)
	{
		file >> first;
		file >> second;
		cameras[first]->matchable.insert(cameras[first]->matchable.begin(), 1, make_pair(second, 10));
		if(cameras[first]->scaleRatio.count(second) <= 0)
		{
			cameras[first]->scaleRatio[second] = 1;
		}
		number--;
	}
	file.close();
}

void changeNumberOfPairs(string fileName)
{
	ifstream file(fileName);
	if(!file)
	{
		cout << "File doesn't exist" << endl;
		return;
	}
	int number, first, second;
	file >> number;
	while(number > 0)
	{
		file >> first;
		file >> second;
		if(second > cameras[first]->matchable.size()) second = cameras[first]->matchable.size();
		cameras[first]->numberOfPairedImages = second;
		if(cameras[first]->numberOfPairedImages == 0) cameras[first]->excluded = true;
		number--;
	}
	file.close();	
}



void findImagePairs(int way, float angleMin, float angleMax, float scaleMin, float scaleMax)
{
	pair<float, float> result;
	if(way == 0)
	{
		for(int i = 0; i < cameras.size() - 1; i++)
			for(int j = i + 1; j < cameras.size(); j++)
			{
				result = averageAngleAndScale(cameras[i], cameras[j]); 
				if(result.first != result.first || result.second != result.second)  continue;
				if(result.second >= scaleMin && result.second <= scaleMax)
				{
					if(result.first >= angleMin  && result.first <= angleMax)
					{
						float daisyAngle = daisyRotation(cameras[i], cameras[j]);
						if(daisyAngle != daisyAngle) continue;
						cameras[i]->matchable.push_back(make_pair(j, daisyAngle + result.first));
						cameras[i]->scaleRatio[j] = result.second;
						cameras[j]->matchable.push_back(make_pair(i, daisyAngle + result.first));
						cameras[j]->scaleRatio[i] = 1/result.second;
					}
					cameras[i]->consistencyCheck.push_back(make_pair(j, result.first));
					cameras[j]->consistencyCheck.push_back(make_pair(i, result.first));
				}
			}
	}
	else if(way == 1)
	{
		for(int i = 0; i < cameras.size() - 1; i++)
			for(int j = i + 1; j < cameras.size(); j++)
			{
				result = averageAngleAndScale(cameras[i], cameras[j]);
				if(result.first != result.first || result.second != result.second)  continue;
				if(result.second >= scaleMin && result.second <= scaleMax)
				{
					if(result.first >= angleMin  && result.first <= angleMax)
					{
						cameras[i]->matchable.push_back(make_pair(j, result.first));
						cameras[i]->scaleRatio[j] = result.second;
						cameras[j]->matchable.push_back(make_pair(i, result.first));
						cameras[j]->scaleRatio[i] = 1/result.second;
					}
					cameras[i]->consistencyCheck.push_back(make_pair(j, result.first));
					cameras[j]->consistencyCheck.push_back(make_pair(i, result.first));	
				}
			}		
	}
	else
	{
		for(int i = 0; i < cameras.size() - 1; i++)
			for(int j = i + 1; j < cameras.size(); j++)
			{
				result = averageAngleAndScale(cameras[i], cameras[j]);
				if(result.first != result.first || result.second != result.second)  continue;
				if(result.second >= scaleMin && result.second <= scaleMax)
				{
					if(result.first >= angleMin  && result.first <= angleMax)
					{
						cameras[i]->matchable.push_back(make_pair(j, abs(1 - result.second)));
						cameras[i]->scaleRatio[j] = result.second;
						cameras[j]->matchable.push_back(make_pair(i, abs(1 - result.second)));
						cameras[j]->scaleRatio[i] = 1/result.second;
					}
					cameras[i]->consistencyCheck.push_back(make_pair(j, result.first));
					cameras[j]->consistencyCheck.push_back(make_pair(i, result.first));					
				}
			}			
	}
	for(int i = 0; i < cameras.size(); i++) cameras[i]->sortPairs();
}