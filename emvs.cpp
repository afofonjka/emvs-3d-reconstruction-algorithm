#include <iostream>
#include <cfloat>
#include "consistencyMeasure.cpp"

using namespace std;

void *measureConsistencyThread(void *param)
{
  ConsistencyParameters *par = (ConsistencyParameters*)param;
  int id = par->image;
  int curr;
  int writeMod = ceil(0.1 * cameras.size());
  while(1)
  {
    mtxx.lock();
    curr = currentImage;
    currentImage++;
    if(curr != 0 && (curr % writeMod) == 0 && curr < cameras.size()) cout << (curr/writeMod) * 10 << " \%"<< endl;
    mtxx.unlock();
    if(curr < cameras.size())
    {
     	measureConsistency(curr, par, id);
    } 
    else break;
  }
  pthread_exit(NULL);
}



void *firstCloud(void *data)
{
  Information *inf = (Information*)data;
  int i, j;
  while(1)
  {
    mtxx.lock();

    i = currentRow;
    j = currentCol;
    if(currentCol + 1 == inf->numCols)
    {
      currentCol = 0;
      currentRow++;
    }
    else
    {
      currentCol++;
    }
    mtxx.unlock();
    if(i >= inf->numRows) 
    {
      pthread_exit(NULL);
    }
    inf->row = i;
    inf->col = j;
    computeFirstCloud(inf);
  }
}



void createFirstCloud(int image, int rows, int cols, int numThreads)
{
  if(cameras[image]->bestPair == -1)
  {
    return; 
  }
  prepareImagePair(image, daisyRad, daisyRadq, daisyThq, daisyHistq);
  currentRow = 0;
  currentCol = 0;
  pthread_t *threads = (pthread_t*)malloc(numThreads * sizeof(pthread_t));
  int rc;
  vector<Information*> informations;
  for(int i = 0; i < numThreads; ++i)
  {
      Information *inf = new Information(i, image, i, i, rows, cols, daisyRad, numberOfPoints, gridUnitSize, probabilitySigma, probabilityRatio, probabilityRatioSparse, descriptorSize, cameras[image]->w, cameras[image]->h, cameras[cameras[image]->bestPair]->w, cameras[cameras[image]->bestPair]->h);
      rc = pthread_create(&threads[i], NULL, firstCloud, (void *)inf);
      informations.push_back(inf);    
  }
  void *status;
  int counter = 0;
  for(int i = 0; i < numThreads; ++i)
  {
      rc = pthread_join(threads[i], &status);
      counter += informations[i]->counter;
      if (rc)
      {
          cout << "Error:unable to join," << rc << endl;
          exit(-1);
      }
  }
  saveMatrixToBinary(saveFolder, image, cameras[image]->w, cameras[image]->h);
}



void measureConsistencyParallel(string folder, int maxNumberOfImages, float ratio, float epsilon, int nThreads, int minImages, int octreeDepth)
{
  time_t begin, end;
  double et;
  begin = time(NULL);
  currentImage = 0;
  maximums = (float**)malloc(nThreads * sizeof(float*));
  minimums = (float**)malloc(nThreads * sizeof(float*));
  pthread_t *threads = (pthread_t*)malloc(nThreads * sizeof(pthread_t));
  mutexArray = (pthread_mutex_t*)malloc(cameras.size() * sizeof(pthread_mutex_t));
  int rc;
  void *status;
  vector<ConsistencyParameters*> parameters; 
  totalSize = 0;
  float distance = 0;
  for(int i = 0; i < nThreads; ++i)
  {
    maximums[i] = (float*)malloc(3*sizeof(float));
    maximums[i][0] = -1000000;
    maximums[i][1] = -1000000;
    maximums[i][2] = -1000000;
    minimums[i] = (float*)malloc(3*sizeof(float));
    minimums[i][0] = 1000000;
    minimums[i][1] = 1000000;
    minimums[i][2] = 1000000;
    ConsistencyParameters *param = new ConsistencyParameters(i, maxNumberOfImages, ratio, epsilon, nThreads, minImages);
    rc = pthread_create(&threads[i], NULL, measureConsistencyThread, (void *)param);
    parameters.push_back(param);
    if(rc)
    {
      cout << "Error:unable to create thread " << rc << endl;
      exit(-1);     
    }
  }
  for(int i = 0; i < nThreads; i++) 
  {
    rc = pthread_join(threads[i], &status);
    totalSize += parameters[i]->nodes.size();
    if (rc)
    {
          cout << "Error:unable to join thread " << rc << endl;
          exit(-1);
    }
  }  
  cout << "Creating octree out of " << totalSize << " points" << endl;
  createOctree(&parameters, octreeDepth);
  end = time(NULL);
  et = double(end - begin);
  cout << "Consistency time: " << et << " s" << endl;
}




int main(int argc, char **argv)
{
  string imageFolder;
  char* parameters;
  char* pairs;
  char *npairs;
  bool inputFolder = false;
  bool inputParam = false;
  bool inputPairs = false;
  bool inputNumberOfPairs = false;
  int read = 1;
  int curr;
  while(read < argc)
  {
    if(strcmp(argv[read], "-folder") == 0 && (read + 1) < argc)
    {
      inputFolder = true;
      imageFolder = argv[read + 1];
      read += 2;
    }
    else if(strcmp(argv[read], "-param") == 0 && (read + 1) < argc)
    {
      inputParam = true;
      parameters = argv[read + 1];
      read += 2;
    }
    else if(strcmp(argv[read], "-pairs") == 0 && (read + 1) < argc)
    {
      inputPairs = true;
      pairs = argv[read + 1];
      read += 2;
    }
    else if(strcmp(argv[read], "-npairs") == 0 && (read + 1) < argc)
    {
      inputNumberOfPairs = true;
      npairs = argv[read + 1];
      read += 2;
    }
    else
    {
      read += 2;
      cout << "Unrecognized command-line parameter" << endl;
    }
  }
  if((!inputFolder) || (!inputParam))
  {
    cout << "Please input image folder and parameters file" << endl;
    return 0;    
  }
  loadParameters(parameters);
  saveFolder = imageFolder + "/" + saveFolder;
  system(("mkdir " + saveFolder + " &> /dev/null").c_str());
  system(("mkdir " + saveFolder + "/depth_maps" + " &> /dev/null").c_str());
  system(("mkdir " + saveFolder + "/result" + " &> /dev/null").c_str());
  system(("mkdir " + saveFolder + "/temp" + " &> /dev/null").c_str());
  time_t BEGIN = time(NULL);
  time_t begin, end;
  double et;
  begin = time(NULL);
  load_bundler(bundlerFile);
  load_images(listImages, cameras.size());
  end = time(NULL);
  et = double(end - begin);
  cout << "Loading time: " << et << " s"<< endl;
  begin = time(NULL);
  findImagePairs(pairMethod, minAngle, maxAngle, minScale, maxScale);
  if(inputPairs)
  {
    cout << "Reading image pairs from file" << endl;
    changeImagePairs(pairs);
  }
  if(inputNumberOfPairs)
  {
    cout << "Reading number of paired images from file" << endl;
    changeNumberOfPairs(npairs);
  }
	end = time(NULL);
	et = double(end - begin);
	cout << "Pairing images: " << et << " s"<<endl;
  begin = time(NULL);
  cout << "-----------------------------------" << endl;
  cout << "Dense point clouds: " << endl;
  if(!onlyConsistency)
  {
    for(int i = 0; i < cameras.size(); ++i)
    {
      if(!cameras[i]->excluded)
      {
        read = 0;
        curr = 0;
        while(read < cameras[i]->numberOfPairedImages)
        {
          begin = time(NULL);
          if(cameras[i]->matchable.size() < (curr + 1))
          {
            cout << "Image " << i << " doesn't have enough close images" << endl;
            cameras[i]->numberOfPairedImages = cameras[i]->matchable.size();
            break;  
          }
          while(cameras[cameras[i]->matchable[curr].first]->excluded)
          {
            curr++;
          }
          if(cameras[i]->matchable.size() < (curr + 1))
          {
            cout << "Image " << i << " doesn't have enough close images" << endl;
            cameras[i]->numberOfPairedImages = cameras[i]->matchable.size();
            break;  
          }
          cameras[i]->bestPair = cameras[i]->matchable[curr].first;
          createFirstCloud(i, threadsHeight, threadsWidth, numberOfThreads);
          end = time(NULL);
          et = double(end - begin);
          cout << " time: " << et << " s" <<  endl;
          read++;
          curr++;
        }
      }
    }
  }
  cout << "-----------------------------------" << endl;
  cout << "Consistency computation" << endl;
  if(useConsistencyThreshold)
  {
    for(int i = 0; i < cameras.size(); ++i) cameras[i]->myEpsilon = consistencyThreshold;
  }
  else cout << "Discretization error " << ": " << cameras[0]->myEpsilon << endl;
  measureConsistencyParallel(saveFolder, numImagesToCheck, imagesFactor, consistencyThreshold, numberOfThreads, minNumberOfImages, octreeDepth);
  time_t END = time(NULL);
  cout << "Total EMVS time: " << double(END - BEGIN) << " s" << endl;
  remove(("rm " + imageFolder + "/" + saveFolder + "/temp/0.pgm").c_str());
  remove(("rm " + imageFolder + "/" + saveFolder + "/temp/1.pgm").c_str());
  remove(("rm " + imageFolder + "/" + saveFolder + "/temp").c_str());
  return 0;
}



