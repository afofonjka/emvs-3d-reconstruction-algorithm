ABOUT:

EMVS (Efficient multi-view stereo) is an approach for multi-view stereo matching especially designed to operate on high-resolution image sets and to efficiently compute dense point clouds [1]. The key is in the robust DAISY descriptor used to measure similarities across images that can be computed very efficiently for every single image point in every direction [2].


1. Pairwise point clouds

This step consists of computing dense point clouds from image pairs. The cloud from one image pair is computed using DAISY descriptor to measure similarity along epipolar lines. 

2. Enforcing consistency

This step is needed because the point clouds computed in the previous step may contain a few spurious points and because one point can be instantiated form several different image pairs. Thus, for optimum results, it is important to retain only the points with the highest expected precision for which there is a consistent evidence in multiple pairs.


[1] E. Tola et al., Efficient large-scale multi-view stereo for ultra high-resolution image sets, Mac. Vis. and Appl. 23 (5), 903 (2012). 
[2] E. Tola, V. Lepetit, P. Fua: Daisy: an efficient dense descriptor applied to wide baseline stereo, IEEE Trans. Pattern Anal. Mach. Intell. 32 (5), 815-830 (2010) 

COMPILATION:

To run the code there are multiple libraries that need to be installed:

1. Octree implementation: https://nomis80.org/code/octree.html

2. Daisy descriptor: https://www.epfl.ch/labs/cvlab/software/descriptors-and-keypoints/daisy/

3. LAPACK + CBLAS: http://www.netlib.org/lapack/

4. ANN library: https://www.cs.umd.edu/~mount/ANN/

5. Eigen library: http://eigen.tuxfamily.org/index.php?title=Main_Page

6. OpenCV: https://opencv.org

Remember to adjust the paths in the Makefile before compiling.

USAGE:

The details about the command line arguments can be found in Options.pdf file.


