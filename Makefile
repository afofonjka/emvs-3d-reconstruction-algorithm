#EMVS makefile

CXX = g++

OCTREE_PATH=/Users/fofonjka/Software_packages/octree_code

DAISY_PATH=/Users/fofonjka/Software_packages/daisy-1.8.1

LAPACK_PATH=/Users/fofonjka/Software_packages/lapack

ANN_PATH=/Users/fofonjka/Software_packages/ann_1.1.2

EIGEN_PATH=/Users/fofonjka/Software_packages/Eigen

#YOUR_INCLUDE_PATH = -I/Users/anamarija/Documents/EMVS/lib -I/Users/anamarija/Documents/EMVS/lib/octree -I/Users/anamarija/Documents/EMVS/lib/CBLAS/include -I/Users/anamarija/Documents/Meshlab/meshlab/src/meshlabplugins/edit_cordpaint/dependencies/OpenCV231_static/include -I/Users/anamarija/Documents/installations/daisy-1.8.1/include/ -I/Users/anamarija/Documents/installations/ann_1.1.2/include/ 

INCLUDE_PATH = -I${EIGEN_PATH}/ -I${LAPACK_PATH}/CBLAS/include/ -I${LAPACK_PATH}/LAPACKE/include/ -I ${ANN_PATH}/include/ -I ${OCTREE_PATH}/ -I ${DAISY_PATH}/include/ -I /usr/local/Cellar/opencv/2.4.9/include/

#YOUR_LDLIB_PATH = -L/Users/anamarija/Documents/installations/opencv-2.4-2.10/installation/3rdparty/lib -L/Users/anamarija/Documents/installations/opencv-2.4-2.10/installation/lib -L/Users/anamarija/Documents/EMVS/lib -L/Users/anamarija/Documents/EMVS/lib/CBLAS/lib -L/Users/anamarija/Documents/Meshlab/meshlab/src/meshlabplugins/edit_cordpaint/dependencies/OpenCV231_static/lib -L/usr/X11/lib -L/Users/anamarija/Documents/installations/daisy-1.8.1/lib/ -L/Users/anamarija/Documents/installations/ann_1.1.2/lib/

#maybe modify a bit the open cv paths
LIBRARY_PATH = -L ${DAISY_PATH}/lib/ -L${LAPACK_PATH}/ -L ${ANN_PATH}/lib/ -L /usr/local/Cellar/jpeg/8d/lib/

CXXFLAGS =  ${INCLUDE_PATH} ${LIBRARY_PATH} -lcblas -lANN -lblas -ldaisy -lopencv_highgui -lopencv_core 

emvs: emvs 

