#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>

using namespace std;

//replaces f with factor*f

void load_bundler(string infile, string outfile, float factor)
{
	ifstream file(infile);
	ofstream outFile(outfile);
	if(!file)
	{
		cout << "Input bundler file doesn't exist" << endl;
		exit(-1);
	}
	if(!outFile)
	{
		cout << "Output bundler file doesn't exist" << endl;
		exit(-1);
	}
	string line;
	getline(file,line);
	int numCameras, numPoints, temp1, camera;
	float focal, d1, d2;
	int t1, t2, t3;
	file >> numCameras;
	file >> numPoints;
	cout << "Reading cameras and points: " << numCameras << " " << numPoints << endl;
	outFile << line << endl;
	outFile << numCameras << " " << numPoints << endl;
	for(int i = 0; i < numCameras; i++)
	{
		//focal length + camera distortion parameters
		file >> focal;
		file >> d1;
		file >> d2;
		focal = factor * focal;
		outFile << setprecision(10) << scientific << focal << " " << d1 << " " << d2 << endl;  
		//rotation matrix
		for(int j = 0; j < 3; j++)
		{
			file >> focal;
			file >> d1;
			file >> d2;
			outFile << focal << " " << d1 << " " << d2 << endl;
		}
		//translation vector
		file >> focal;
		file >> d1;
		file >> d2;
		outFile << focal << " " << d1 << " " << d2 << endl;		
	}
	for(int i = 0; i < numPoints; i++)
	{
		//position
		file >> focal;
		file >> d1;
		file >> d2;
		outFile << setprecision(4) << fixed << focal << " " << d1 << " " << d2 << endl;
		//color
		file >> t1;
		file >> t2;
		file >> t3;
		outFile << t1 << " " << t2 << " " << t3 << endl;
		//view list
		file >> t1;
		outFile << t1;
		for(int j = 0; j < t1; j++)
		{
			file >> t2;
			file >> t3;
			file >> d1;
			file >> d2;
			outFile << " " << t2 << " " << t3 << " " << d1 << " " << d2; 
		}
		outFile << endl;
	}
	file.close();
	outFile.close();
	return;
}





int main(int argc, char **argv)
{
	if(argc < 4)
	{
		cout << "Input parameters:" << endl;
		cout << "Input_file Output_file factor" << endl; 
	}
	else
	{
		load_bundler(argv[1], argv[2], atof(argv[3]));
	}
	return 0;
}